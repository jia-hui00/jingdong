function get_hot_words() {
    return Mock.mock({
        'result|7-10': [{
            word: '@cword(2,5)',
        }]
    })
}

function get_menu() {
    return Mock.mock({
        'data|18': [{
            'title|2-3': ['@cword(2,4)'],
            'content': {
                'tabs|2-5': ['@cword(2,4)'],
                'details|8-15': [{
                    'category': '@cword(2,4)',
                    'items|8-16': ['@cword(2,4)']
                }]
            }
        }]
    })
}
