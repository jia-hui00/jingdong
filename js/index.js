$(function () {
    (function () {
        function init() {
            // 获取热词
            var hot_words = get_hot_words()
            render_hotwords(hot_words.result);
            hot_timer = setInterval(function () {
                render_search_word();
            }, 3000);
            bind_event();
        }
        // 热词定时器
        var hot_timer
        // 查询延时器
        var search_timer
        // 联想框延时器
        var association
        init();
        function bind_event() {
            $(".header .search-inp").hover(
                function () {
                    clearInterval(hot_timer);
                },
                function () {
                    hot_timer = setInterval(render_search_word, 3000);
                }
            );
            // 触发联想事件
            $(".header .search-inp").on('input', function () {
                var value = $(this).val().trim()
                if (!value) return;
                clearTimeout(search_timer)
                search_timer = setTimeout(function () {
                    $.ajax({
                        url: 'https://suggest.taobao.com/sug',
                        data: {
                            code: 'utf-8',
                            q: value,
                            callback: 'searchCB'
                        },
                        dataType: 'jsonp'
                    })
                }, 700)
            })
            $('.header .search-words').mouseleave(function () {
                association = setTimeout(function () {
                    $('.header .search-words').hide()
                }, 1000)
            }).mouseenter(function () {
                clearTimeout(association)
            })
            // logo 切换
            $('.header .logo').hover(function () {
                $(this).find('.img1').hide('slow')
                $(this).find('.img2').show('slow')
            }, function () {
                $(this).find('.img1').show('slow')
                $(this).find('.img2').hide('slow')
            })
        }
        // 回调函数，绑定到 window 上
        window.searchCB = function (resp) {
            console.log(resp);
            var html_str = '<li>未搜索到任何商品</li>'
            if (resp.result.length !== 0) {
                html_str = resp.result.map(item => `
                            <li>
                                <span class="goods-name"><a href="">${item[0]}</a></span>
                                <span class="goods-count">约${parseInt(item[1])}件商品</span>
                            </li>`).join('')
            }
            $('.header .search-words').html(html_str).show()
        }
        function render_hotwords(resp) {
            var html_str = resp.map((item) =>
                `<li><a href="">${item.word}</a></li>`).join("");
            $(".header .search .hotwords").html(html_str);
        }
        function render_search_word() {
            var hot_words = get_hot_words()
            $(".header .search-inp")[0].placeholder = hot_words.result[0].word;
        }
    })();
    (function () {
        var data_list = [];
        function init() {
            var menu = get_menu()
            data_list = menu.data;
            render_menu();
            bind_event()
        }
        init()
        function bind_event() {
            $('.fs-1 .menu li').mouseenter(function () {
                $(this).addClass('active').siblings().removeClass('active')
                render_content($(this).index())
                $('.fs-1 .menu-content').show()
            })
            $('.fs').mouseleave(function () {
                $(this).find('.active').removeClass('active')
                $(this).find('.menu-content').hide()
            })
        }
        function render_menu() {
            var menu = "";
            data_list.map(function (item) {
                menu += "<li>" + item.title
                    .map((title) => `<a href="">${title}</a>`)
                    .join("<span>/</span>") + "</li>";
            });
            $(".fs-1 .menu").html(menu);
        }
        function render_content(index) {
            var content = data_list[index].content
            var tabs = content.tabs.map(tab => `<span>${tab}</span>`).join('')
            $('.fs-1 .menu-title').html(tabs)
            var details = content.details.map(function (detail) {
                var dt = `<dt>${detail.category}</dt>`
                var dd = detail.items.map(function (item) {
                    return `<dd><a href="">${item}</a></dd>`
                }).join('')
                return '<dl>' + dt + dd + '</dl>'
            })
            $('.fs-1 .details').html(details)
        }
    })();
    $('.fs .fs-2 .swiper1').swiper({
        contents: $('.fs-2-img'),
        toggle_time: 2500,
        type: 'fade',
        is_auto: true,
        show_toggle_btn: 'always',
        show_spots: true,
        spots_size: 10,
        spots_position: 'left',
        spots_color: '#ccc8',
        cur_spot_color: '#008c8c',
    })
    $('.fs .fs-2 .swiper2').swiper({
        contents: $('.swiper2 .items'),
        toggle_time: 2500,
        type: 'slide',
        is_auto: true,
        show_toggle_btn: 'hover',
        show_spots: false,
    })
    $('.fs-3 .font').mouseenter(function () {
        $('.fs-3 .service_list').animate({ 'margin-top': -33 }, 800)
        $('.fs-3 .service_pop').animate({ bottom: 8 }, 800)
        $('.fs-3 .underline').hide().eq($(this).index()).show()
        if ($(this).hasClass('font_')) {
            $('.fs-3 .font:eq(0) span').text('游戏')
        }
    })
    $('.fs-3 .close').click(function () {
        $('.fs-3 .service_list').animate({ 'margin-top': 0 }, 800)
        $('.fs-3 .service_pop').animate({ bottom: -210 }, 800)
        $('.fs-3 .service .underline').hide()
        $('.fs-3 .font:eq(0) span').text('话费')
    })
    $(".seckill .swiper3").swiper({
        contents: $(".seckill .seckill-item"),
        toggle_time: 2500,
        type: "animate",
        is_auto: false,
        show_toggle_btn: "hover",
        show_spots: false,
        height: 260
    });
    var end_time = +new Date() + 10000000;
    function calculate_time() {
        var now_time = +new Date();
        var dis_time = end_time - now_time;
        var hour = parseInt(dis_time / 1000 / 60 / 60);
        var minute = parseInt((dis_time / 1000 / 60) % 60);
        var second = parseInt((dis_time / 1000) % 60);
        if (hour < 10) hour = "0" + hour;
        if (minute < 10) minute = "0" + minute;
        if (second < 10) second = "0" + second;
        $('.seckill .hour').text(hour)
        $('.seckill .minute').text(minute)
        $('.seckill .second').text(second)
    }
    setInterval(calculate_time, 1000)
})